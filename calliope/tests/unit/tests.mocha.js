describe('HUGE Unit Test', function(){
	var navigation;
	beforeEach(function(){
		navigation = JSON.parse('[{"label":"Work","url":"#/work","items":[]},{"label":"About","url":"#/about","items":[{"label":"What we do","url":"#/about/what-we-do"},{"label":"How we work","url":"#/about/how-we-work"},{"label":"Leadership","url":"#/about/leadership"}]},{"label":"Careers","url":"#/careers","items":[{"label":"Client Services","url":"#/careers/client-services"},{"label":"Creative","url":"#/careers/creative"},{"label":"Motion & Media","url":"#/careers/motion-and-media"},{"label":"Operations","url":"#/careers/operations"},{"label":"People","url":"#/careers/people"},{"label":"Strategy","url":"#/careers/strategy"},{"label":"Technology","url":"#/careers/technology"},{"label":"UX & Product Design","url":"#/careers/ux-and-product-design"}]},{"label":"Ideas","url":"#/ideas","items":[{"label":"Reports","url":"#/ideas/reports"},{"label":"Perspectives","url":"#/ideas/perspectives"},{"label":"Books","url":"#/ideas/books"},{"label":"Videos","url":"#/ideas/videos"}]},{"label":"News","url":"#/news","items":[]},{"label":"Events","url":"#/events","items":[]},{"label":"Contact","url":"#/contact","items":[{"label":"Atlanta","url":"#/contact/atlanta"},{"label":"Brooklyn","url":"#/contact/brooklyn"},{"label":"DC","url":"#/contact/dc"},{"label":"London","url":"#/contact/london"},{"label":"Los Angeles","url":"#/contact/los-angeles"},{"label":"Portland","url":"#/contact/portland"},{"label":"Rio","url":"#/contact/rio"},{"label":"San Francisco","url":"#/contact/san-francisco"}]}]');
	});

	describe('#createNavElement()', function(){
		it('should returns an item with no submenu', function(){
			var r = HUGE.createNavElement(navigation[0]);
			r.childNodes.length.should.equal(1);
			r.childNodes[0].tagName.should.equal("A");
			r.childNodes[0].href.should.contain("#/work");
		});
		it('should returns an item with no submenu', function(){
			var r = HUGE.createNavElement(navigation[1]);
			console.log(r.childNodes);
			r.childNodes.length.should.equal(3);
			r.childNodes[0].tagName.should.equal("A");
			r.childNodes[2].tagName.should.equal("UL");
			r.childNodes[1].tagName.should.equal("SPAN");
			r.childNodes[2].childNodes.length.should.equal(3);
		});
	});
});