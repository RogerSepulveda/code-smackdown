///////////////////
// Miniframework //
///////////////////
Element.prototype.hasClass = function (className) {
	return (" " + this.className + " ").match(" " + className + " ") !== null;
};
Element.prototype.addClass = function (className) {
	if(!this.hasClass(className)){
		this.className += " "+className;
	}
	return this;
};
Element.prototype.removeClass = function (className) {
	if(this.hasClass(className)){
		this.className = (" " + this.className + " ").replace(" "+ className +" ", "");
	}
	return this;
};
Element.prototype.toggleClass = function (className) {
	if(this.hasClass(className)){
		this.removeClass(className);
	}else{
		this.addClass(className);
	}
	return this;
};
Element.prototype.show = function(value){
	value = value || "block";
	this.style.display = value;
};
Element.prototype.hide = function(){
	this.style.display = "none";
};
//////////////////////////
//End of mini framework //
//////////////////////////



////////////////////
// App Javascript //
////////////////////

(function(){
	var root = this,
		HUGE = root.HUGE = {},
		nav, hamburger, brand, overlay;

	///////////////////////
	// public contracts //
	///////////////////////
	HUGE.init = init;
	HUGE.createNavElement = createNavElement;

	//////////////////////
	// private methods //
	//////////////////////

	/**
	 * Initialization
	 */
	function init(){
		var xmlhttp;
		if (window.XMLHttpRequest){
			xmlhttp = new XMLHttpRequest();
			xmlhttp.open("GET", "/api/nav.json", true);
			xmlhttp.onreadystatechange=callbackNavigation;
			xmlhttp.send();
		}
		nav = document.querySelector("nav");
		navList = document.querySelector("ul.primary-nav");
		hamburger = document.getElementById("hamburger");
		brand = document.getElementById("brand");
		overlay = document.getElementById("overlay");
		overlay.addEventListener("click", onClickOverlay);
		nav.addEventListener("webkitTransitionEnd", closeOverlay);
		nav.addEventListener("transitionend", closeOverlay);
		hamburger.addEventListener("click", onClickHamburguer);
	}
	/**
	 * Callback for the navigation api ajax request
	 * @param  {Object} res Response
	 */
	function callbackNavigation(res){
		if (res.target.readyState==4 && res.target.status==200){
			setNav(JSON.parse(res.target.responseText));
		}		
	}

	/**
	 * Used to close the transparent mask when the transition has ended.
	 */
	function closeOverlay(){
		if(!nav.hasClass("show")){
			overlay.hide();
		}
	}
	/**
	 * Listener called when the user clicks on the hamburger
	 * @param  {Object} event 
	 */
	function onClickHamburguer(event){
		event.stopPropagation();
		if(!hamburger.hasClass("close")){
			brand.addClass("show");
			overlay.show();
		}else{
			brand.removeClass("show");
		}
		nav.toggleClass("show");
		hamburger.toggleClass("close");

	}
	/**
	 * Listener called when the user clicks on the transparent mask
	 * @param  {Object} event 
	 */
	function onClickOverlay(event){
		console.log(event);
		nav.toggleClass("show");
		hamburger.toggleClass("close");
		if(isMobile()){
			brand.removeClass("show");
		}else{
			var active = document.querySelector("li.active");
			active.removeClass("active");
			overlay.hide();
		}
	}
	/**
	 * Pupulate the navigation unordered list with the JSON obtained.
	 * @param {Object} objNav Object parsed from the JSON return by the service API.
	 */
	function setNav(objNav){
		var i = 0, 
			length = objNav.items.length,
			item; 
		for(; i < length ; i++){
			item = objNav.items[i];
			navList.appendChild(createNavElement(item));
		}
	}
	/**
	 * Create each LI element of the navigation recursively.
	 * @param  {Object} item Navigation item
	 */
	function createNavElement(item){
		var li = document.createElement("LI"),
			anchor = document.createElement("A"),
			items = item.items,
			deepItem;
		anchor.appendChild(document.createTextNode(item.label));
		anchor.setAttribute("title", item.label);
		anchor.href = item.url;
		li.appendChild(anchor);
		if(items && items.length){
			var i = 0, 
				length = items.length, 
				ul = document.createElement("UL"),
				chevron = document.createElement("SPAN");
				ul.addClass("secondary-nav");
				chevron.addClass("chevron");
				chevron.addClass("bottom");
			for(; i < length ; i++){
				deepItem = items[i];
				ul.appendChild(createNavElement(deepItem));
			}
			li.appendChild(chevron);
			li.appendChild(ul);	
			addClick(anchor, false);
		}else{
			addClick(anchor, true);
		}
		return li;
	}
	/**
	 * Add the event listener to every element in the list, if it is a terminal item then change the location.href, else shows the transparent mask.
	 * @param {anchor} anchor    <A> navagation element
	 * @param {Boolean} isTerminal If it is a terminal element of the navigation
	 */
	function addClick(anchor, isTerminal){
		anchor.addEventListener("click", function(event){
			event.preventDefault();
			var elem = event.target.parentNode,
				active = elem.parentNode.querySelector("li.active");
			elem.toggleClass("active");
			active && active.removeClass("active");
			if(!isTerminal){
				overlay.show();
			}else{
				location.href = anchor.href;
			}
		});
	}
	/**
	 * Determines the screen width z-index based.
	 * @return {Boolean}
	 */
	function isMobile(){
		var s = document.defaultView.getComputedStyle(document.querySelector("header"), '');
		return s.zIndex == "1041";
	}
}).call(this);