var App = window.App || {};

/**
 * process the menu data to buil it
 * @return {bolean} true if it was sucesful
 */
App.procMenu = function(json){
	App.menuItems = JSON.parse(json);
	App.renderLevel(App.menuItems, App.menuContainer);
	App.clickManagment();
	var menutrigger = document.querySelector('.mobile-trigger');
	menutrigger.addEventListener('click', App.overlay);
	document.querySelector('.overlay').addEventListener('click', function(){
		menutrigger.click();
	});
}
/**
 * render each level of menu
 * @param  {JSON} menu      the current level
 * @param  {DOM object} container the place to put the current level
 */
App.renderLevel = function(menu, container, deep){

	var deep = deep || 0,
		level = document.createElement('ul'),
		anchor = document.createElement('a'),
		hasLabel = (typeof menu.label != 'undefined'),
		hasAnchor = (typeof menu.url != 'undefined');

	if(hasLabel) {
		var text = document.createTextNode(menu.label);
		anchor.appendChild(text);
		container.appendChild(anchor);
		if(hasAnchor) anchor.href = menu.url;
	}


	if((typeof menu.items != 'undefined') && menu.items.length && (menu.items.length > 0))  {
		container.appendChild(level);
		container.className = container.className + " sub-menu close";
		for (var ndx in menu.items) {
			var element = document.createElement('li'),
				item = menu.items[ndx];
			element.className = "level-" + deep + " " + 'menu-item';
			level.appendChild(element);
			App.renderLevel(item, element , deep + 1);
		}
	}

}

/**
 * the AJAX method
 * @return {object} the AJAX response
 */
App.ajax = function(url, callback){
    var xmlHTTP;

    xmlHTTP = new XMLHttpRequest();
    xmlHTTP.onreadystatechange = function(){
        if (xmlHTTP.readyState == 4 && xmlHTTP.status == 200){
        	if (typeof callback == 'function'){
	            callback(xmlHTTP.responseText);
	        }
        }
    }
    xmlHTTP.open("GET", url, true);
    xmlHTTP.send();
}

/**
 * manage the sub menu click in mobile
 */
App.clickManagment = function(){
	var el = document.getElementById('mainMenu').getElementsByClassName('sub-menu');
	for (var ndx in el) {
		if(ndx == 'length') break;
		var current = el[ndx];
		current.querySelector('a').addEventListener('click', App.clickProcess);
	}
}

/**
 * process the click in mobile layout
 * @param  {event} the current even
 */
App.clickProcess = function(e){
	if(window.innerWidth > 768) return true;
	if(e.srcElement.className != "active"){
		e.preventDefault();
		App.resetAnchors();
		e.srcElement.className = "active";
		App.removeClass(e.srcElement.parentNode, 'close');
		App.addClass(e.srcElement.parentNode, 'open');
	}
}

/**
 * reset the menu anchor 
 */
App.resetAnchors = function(){
	var el = document.getElementById('mainMenu').getElementsByClassName('sub-menu');
	for (var ndx in el) {
		if(ndx == 'length') break;
		var current = el[ndx];
		App.removeClass(current, 'open');
		App.addClass(current, 'close');
		current.querySelector('a').className = "";
	}
}

// CSS classes magnament

  App.hasClass = function(obj,cls) {
	return obj.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
  }

  App.addClass = function(obj,cls) {
   if(!App.hasClass(obj,cls)) {
    obj.className += " " + cls;
   }
  }

  App.removeClass = function(obj,cls) {
   if(App.hasClass(obj,cls)) {
    var exp = new RegExp('(\\s|^)'+cls+'(\\s|$)');
    obj.className = obj.className.replace(exp,"");
   }
  }

App.overlay = function(){
	var overlay = document.querySelector('.overlay');
	if(window.innerWidth > 768) return true;
	if(App.hasClass(overlay, 'show')) {
		App.removeClass(overlay, 'show');
	} else {
		App.addClass(overlay, 'show');
	}
}

/**
 * initialize de app
 */
App.init = (function(){
	App.menuContainer = document.getElementById('mainMenu');
	App.dataMenuUrl = '/data/nav.json';
	App.ajax(App.dataMenuUrl, App.procMenu);
})();
