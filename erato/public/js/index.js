/**
 * Initializes all the componentes needed by the page
 */
function initApp() {

  loader.initialize();
  menu.initialize({
    loader: loader,
    el: document.getElementById('nav-bar')
  });

};
