/**
 * Definition of the custom menu for NavExercise
 * @author: David Gomez Fonnegra
 * @github-user: lowlander1982
 */
 var menu = {

  /**
   * Initializes option attributes, events, models and collections if needed
   * @param {Object} options, initialization attributes
   * @public
   */
  initialize: function(options) {
    this.options = options || {};
    this.loader = this.options.loader;
    this.el = this.options.el;

    this._initModels();
  },

  /**
   * Initializes the models
   * @private
   */
  _initModels: function() {
    this.menuItems = {};
    loader.createJsonCall('/api/nav.json', this._buildMenuItems, this);
  },

  /**
   * Attaches the events with the DOM elements
   * @private
   */
  _attachEvents: function() {
    var menu = this.el.getElementsByClassName('top-item'),
      submenu = this.el.getElementsByClassName('bottom-item'),
      i;

    for (i = 0; i < menu.length; i ++) {
      menu[i].onclick = this._checkForItems.bind(this, menu[i]);
    }
    for (i = 0; i < submenu.length; i ++) {
      submenu[i].onclick = this._checkForItems.bind(this, submenu[i]);
    }
    document.body.onclick = this._hideSubMenu.bind(this);
    document.getElementById('show-menu').onclick =
      this._slideBar.bind(this, 80);
    document.getElementById('hide-menu').onclick = this._hideBar.bind(this, 0);
  },

  /**
   * Checks if the menu item has subitems to show them or navigate to the href
   * instead
   * @private
   */
  _checkForItems: function(item, event) {
    var subItems = item.getElementsByTagName('ul'),
      chevron = item.getElementsByClassName('chevron');

    event.stopPropagation();
    this._hideSubMenu();
    if (!subItems.length) {
      location.href = item.getAttribute('data-ref');
    } else {
      this._toggleBottomLevel(true, subItems, []);
      chevron[0].className = 'chevron-close';
    }
  },

  /**
   * Builds the menu items with the json parameter
   * @param {Object} json, object with json of menu items
   * @private
   */
  _buildMenuItems: function(json) {
    this.menuItems = json || {};
    if (this.options.el) {
      this.options.el.innerHTML = this._createMarkup();
    }
    this._attachEvents();
  },

  /**
   * Hides all submenus and reset the chevrons
   * @private
   */
  _hideSubMenu: function() {
    this._toggleBottomLevel(false,
      this.el.getElementsByClassName('bottom-level'),
      this.el.getElementsByClassName('chevron-close'));
  },

  /**
   * Creates the markup for the menu
   * @returns {String} with html of the menu
   * @private
   */
  _createMarkup: function() {
    var items = this.menuItems.items || [],
      markup = '';

    items.forEach(function(item) {
      var subMenu = '',
        subItems = item.items || [];

      subItems.forEach(function(subItem) {
        subMenu = subMenu + '<li class="bottom-item" data-ref="' + subItem.url +
          '"><span>' + subItem.label + '</span></li>';
      });
      subMenu = subMenu ?
        '<ul class="bottom-level hide">' + subMenu + '</ul>' : '';
      markup = markup + '<li class="top-item" data-ref="' + item.url +
        '"><span class="' + (subMenu ? 'chevron' : '') + '">' + item.label +
        '</span>' + subMenu + '</li>';
    });

    markup = '<ul class="top-level">' + markup + '</ul>';

    return markup;
  },

  /**
   * Switches between hiding the submenues or show a specific submenu
   * @param {Boolean} show, either show or hide
   * @param {Array} items, items to hide or show
   * @param {Array} chevrons, chevrons to reset to original position
   * @private
   */
  _toggleBottomLevel: function(show, items, chevrons) {
    var i;

    document.getElementById('overlay').className = show ?
      'translucent-overlay' : '';
    for (i = 0; i < items.length; i++) {
      items[i].className = show ? 'bottom-level' : 'bottom-level hide';
    }
    for (i = 0; i < chevrons.length; i++) {
      chevrons[i].className = 'chevron';
    }
  },

  /**
   * Slides the nav-bar from left to right
   * @param {Number} left, with the current left value
   * @private
   */
  _slideBar: function(left) {
    var bar = document.getElementsByClassName('mobile-bar')[0],
      menu = document.getElementsByClassName('nav-bar')[0],
      copyright = document.getElementsByClassName('copyright')[0],
      content = document.getElementsByClassName('content')[0];

    document.getElementById('show-menu').className = 'nav-mobile hide';
    document.getElementById('hide-menu').className = 'nav-mobile';
    document.getElementById('overlay-mobile').className = 'translucent-content';
    menu.className = 'nav-bar';
    copyright.className = 'copyright';
    bar.className = 'mobile-bar';

    if (left > 0) {
      left -= 5;
      menu.style.left = '-' + left + '%';
      copyright.style.left = '-' + left + '%';
      bar.style.left = '-' + left + '%';
      content.style.left = (80 - left) + '%';
      setTimeout(this._slideBar.bind(this, left), 25);
    } else {
      content.className = 'content shifted-content';

      menu.removeAttribute('style');
      copyright.removeAttribute('style');
      bar.removeAttribute('style');
      content.removeAttribute('style');
    }
  },

  /**
   * Slides the nav-bar from right to left
   * @param {Number} left, with the current left value
   * @private
   */
  _hideBar: function(left) {
    var bar = document.getElementsByClassName('mobile-bar')[0],
      menu = document.getElementsByClassName('nav-bar')[0],
      copyright = document.getElementsByClassName('copyright')[0],
      content = document.getElementsByClassName('content')[0];

    document.getElementById('show-menu').className = 'nav-mobile';
    document.getElementById('hide-menu').className = 'nav-mobile hide';

    content.className = 'content';

    if (left < 80) {
      left += 5;
      menu.style.left = '-' + left + '%';
      copyright.style.left = '-' + left + '%';
      bar.style.left = '-' + left + '%';
      content.style.left = (80 - left) + '%';
      setTimeout(this._hideBar.bind(this, left), 25);
    } else {
      menu.removeAttribute('style');
      copyright.removeAttribute('style');
      bar.removeAttribute('style');
      content.removeAttribute('style');

      document.getElementById('overlay-mobile').className = '';

      menu.className = 'nav-bar closed-menu';
      copyright.className = 'copyright closed-menu';
      bar.className = 'mobile-bar closed-bar';
    }
  }

};
