/**
 * Definition of a single threading json loader component
 * @author: David Gomez Fonnegra
 * @github-user: lowlander1982
 */
var loader = {

  xmlHttp: new XMLHttpRequest(),
  queue: [],
  active: false,

  /**
   * Initializes option attributes, events, models and collections if needed
   * @param {Object} options, initialization attributes
   * @public
   */
  initialize: function(options) {
    this.options = options || {};

    this._initEvents();
  },

  /**
   * Initializes the events
   * @private
   */
  _initEvents: function() {
    var self = this;

    this.xmlHttp.onreadystatechange = function() {
      var queueItem;

      if (self.xmlHttp.readyState === 4 && self.xmlHttp.status === 200) {
        self.callback.bind(self.context)(JSON.parse(self.xmlHttp.responseText));

        if (!!self.queue.length) {
          queueItem = self.queue.pop();
          self._loadJson(queueItem.url, queueItem.callback, queueItem.context);
        } else {
          self.active = false;
        }
      }
    };
  },

  /**
   * Checks the queue and configures the json call
   * @param {String}, url, with the json's url
   * @param {Function} callback, with the callback function
   * @param {Object} context, with the context of the callback function
   * @public
   */
  createJsonCall: function(url, callback, context) {
    if (!this.active) {
      this.active = true;
      this._loadJson(url, callback, context);
    } else {
      this.queue.push({
        url: url,
        callback: callback,
        context: context
      });
    }
  },

  /**
   * Calls the ajax call for getting a json
   * @param {String} url, with the json's url
   * @param {Function} callback, with the callback
   * @param {Object} context, with the context of the callback function
   * @private
   */
  _loadJson: function(url, callback, context) {
    this.xmlHttp.open('GET', url, true);
    this.xmlHttp.send();

    this.callback = callback || this._defaultCallback;
    this.context = context || this;
  },

  /**
   * Makes a default callback
   * @param {Object} json, json response
   * @private
   */
  _defaultCallback: function(json) {
    // This method can be override or defined for a default behavior
  }

};
